package com.classes;

import com.annotation.CustomAnnotation;
import com.annotation.FieldAnnotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestClass {

    private static Logger logger = LogManager.getLogger(TestClass.class);

    private TestClass() {
    }

    private TestClass(int a, int b, String c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @FieldAnnotation(className = "TestClass")
    private int customField = 1;

    private int a;
    private int b;
    private String c;

    @CustomAnnotation(city = "Lviv")
    public void customMethod(int a, String b) {
        logger.info("custom method");
    }

    private void someMethod(int a, String b) {
        logger.info(a + " " + b);
    }

    private int getA() {
        return a;
    }

    private int getB() {
        return b;
    }

    private String getString() {
        return c;
    }
}
