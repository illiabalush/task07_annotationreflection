package com.classes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Class clazz = TestClass.class;
        logger.info("\t\tConstructors");
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(Arrays.toString(constructor.getDeclaredAnnotations())).append(" ")
                    .append(constructor.getName()).append(" ")
                    .append(Arrays.toString(constructor.getParameterTypes()));
            logger.info(stringBuilder);
        }
        logger.info("\t\tFields");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(Arrays.toString(field.getDeclaredAnnotations())).append(" ")
                    .append(field.getType()).append(" ")
                    .append(field.getName());
            logger.info(stringBuilder);
        }
        logger.info("\t\tMethods");
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(Arrays.toString(method.getDeclaredAnnotations())).append(" ")
                    .append(method.getReturnType()).append(" ").append(method.getName()).append(" ")
                    .append(Arrays.toString(method.getParameterTypes()));
            logger.info(stringBuilder);
        }

        try {
            Class[] constructorParameters = {int.class, int.class, String.class};
            Constructor constructor = clazz.getDeclaredConstructor(constructorParameters);
            constructor.setAccessible(true);
            TestClass testClass = (TestClass) constructor.newInstance(1, 2, "3");
            testClass.customMethod(1, "");
            Method methodGetA = clazz.getDeclaredMethod("getA");
            methodGetA.setAccessible(true);
            logger.info(methodGetA.invoke(testClass));
            Field fieldA = clazz.getDeclaredField("a");
            fieldA.setAccessible(true);
            fieldA.set(testClass, 3);
            logger.info(methodGetA.invoke(testClass));
            Class[] methodParameters = {int.class, String.class};
            Method methodSomeMethod = clazz.getDeclaredMethod("someMethod", methodParameters);
            methodSomeMethod.setAccessible(true);
            methodSomeMethod.invoke(testClass, 1, "10");
            Method method = clazz.getDeclaredMethod("customMethod", int.class, String.class);
            Annotation[] annotation = method.getDeclaredAnnotations();
            logger.info(Arrays.toString(annotation));
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException
                | InvocationTargetException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
